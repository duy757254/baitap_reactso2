import React, { Component } from "react";

export default class ItemGlasses extends Component {
  render() {
    let { url, name, price } = this.props.data;

    return (
      <div className="col-4">
        <div
          onClick={() => {
            this.props.changeGlasses(this.props.data);
          }}
          className="card card_item text-left pt-3 mb-3"
        >
          <img className=" card-img-top" src={url} alt />
          <div className="card-body">
            <h4 className="card-title text-center">{name}</h4>
            <p className="card-text text-center">Giá: {price}$</p>
          </div>
        </div>
      </div>
    );
  }
}
