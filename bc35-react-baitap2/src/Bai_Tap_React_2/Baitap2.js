import React, { Component } from "react";
import ButtonReset from "./ButtonReset";
import { dataGlasses } from "./dataGlasses";
import ItemGlasses from "./ItemGlasses";
import ModelGlasses from "./ModelGlasses";
import "./Total.css";

export default class Baitap2 extends Component {
  state = {
    glassesArr: dataGlasses,
    position: dataGlasses,
  };
  renderListGlasses = () => {
    return this.state.glassesArr.map((item, index) => {
      return (
        <ItemGlasses
          changeGlasses={this.handleChangeGlasses}
          data={item}
          key={index}
        />
      );
    });
  };
  handleChangeGlasses = (glass) => {
    this.setState({
      position: glass,
    });
  };
  resetForm = () => {
    this.setState({
      position: "",
    });
  };
  render() {
    return (
      <div>
        <ButtonReset reset={this.resetForm} />
        <div className="container container_contain d-flex col-12 ">
          <div className="col-4 ">
            <ModelGlasses glassPos={this.state.position} />
          </div>
          <div className="col-8 row item_glass ">
            {this.renderListGlasses()}
          </div>
        </div>
      </div>
    );
  }
}
