import React, { Component } from "react";

export default class ButtonReset extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => {
            this.props.reset();
          }}
          className="  button_reset"
        >
          Loại bỏ kính
        </button>
      </div>
    );
  }
}
