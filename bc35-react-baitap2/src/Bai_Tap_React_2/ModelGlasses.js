import React, { Component } from "react";
import Model from "./glassesImage/model.jpg";
import "./Total.css";
export default class ModelGlasses extends Component {
  render() {
    let { name, desc, url } = this.props.glassPos;
    return (
      <div className="model">
        <div className="  card text-left">
          <img className="position-relative card-img-top1" src={Model} alt />
          <img className="position-absolute wear_glasses" src={url} />
          <div className=" bg-dark text-light card-body pt-5">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{desc}</p>
          </div>
        </div>
      </div>
    );
  }
}
